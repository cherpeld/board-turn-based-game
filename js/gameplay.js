/** Class representant le jeu */
class GamePlay {
    /**
     *
     * Création du jeu
     *
     * round - tour
     *
     *this.players - Les joueursu
     *this.weapons - Les armes
     *this.bordGame - la map
     *this.initPlacePlayer(); - methode qui place les joueur au début du jeu
     *this.initPlaceWeapon(); - methode qui place les armes au début du jeu
     *this.whoBegin(); - methode aui définit quel joueur commence le jeu
     */
    round;
    constructor(){
        this.weapons = [
            new Weapons("arc", "./images/arc.png", 15),
            new Weapons("revolver", "./images/revolver.png", 20),
            new Weapons("p90", "./images/p90.png", 30),
            new Weapons("fusil", "./images/fusil.png", 25),
        ];
        this.players = [
            new Players("host", "./images/weapons-default/host-default.png", new Weapons("poignard", "./images/poignard.png", 10),100),
            new Players("guest", "./images/weapons-default/guest-default.png", new Weapons("poignard", "./images/poignard.png", 10),100),
        ];
        this.boardGame = new GameBoard("map",10,10);
        this.initPlacePlayer();
        this.initPlaceWeapon();
        this.whoBegin();
        this.isFight = false;
    }

    /**
     *initPlaceWeapon
     *
     * Methode qui place les armes au début du jeu de façon aléatoire
     *
     */
    initPlaceWeapon (){
        for (var i = 0; i < this.weapons.length; i++) {
            let emptyCell = this.boardGame.getEmptyCell();
            this.weapons[i].placeWeapon(emptyCell)
        }
    }

    /**
     *initPlacePlayer
     *
     * Methode qui place les joueurs au début du jeu de façon aléatoire
     *
     */
    initPlacePlayer () {
        for (var i = 0; i < this.players.length; i++) {
            let emptyCell = null;
            while (true){
                emptyCell = this.boardGame.getEmptyCell();
                if (this.players[i].isNotSideOtherPlayers(emptyCell)) {
                    break
                }
            }
            this.players[i].placePlayers(emptyCell)
        }
    }

    /**
     *initPlacePlayer
     *
     * Methode qui déplace les joueurs
     *
     */
    movePlayer (elem) {

        var splitClassName = elem.className;
        splitClassName = splitClassName.split(" ");

        if (splitClassName.includes("cell-authorized")) {

            var oldPosPlayer = document.getElementById(this.currentPlayer.position);

            this.currentPlayer.move(elem);
            this.currentPlayer.cleanAuthorizedCell();


            var clickedWeapon = elem.id;
            var weapon = null;

            //s'il tombe sur une case arme
            if (splitClassName.includes("weapon")) {
                for (var i = 0; i < this.weapons.length; i++) {
                    if (clickedWeapon === this.weapons[i].position) {
                        weapon = this.weapons[i];
                    }
                }
                this.currentPlayer.weapon.placeWeapon(oldPosPlayer); //
                this.currentPlayer.weapon = weapon;
                this.currentPlayer.updateImage();
                elem.innerHTML = "";

                //change le nom de l'arme possédé et ses dégats dans le panel
                let hostWeapon = document.getElementById('host-possessed-weapon');
                hostWeapon.textContent = this.players[0].weapon.name;
                let damageHostWeapon = document.getElementById('host-possessed-weapon-damage');
                damageHostWeapon.textContent = this.players[0].weapon.damage;
                let damageGuestWeapon = document.getElementById('guest-possessed-weapon-guest');
                damageGuestWeapon.textContent = this.players[1].weapon.damage;
                let guestWeapon = document.getElementById('guest-possessed-weapon');
                guestWeapon.textContent = this.players[1].weapon.name;
            }

            this.currentPlayer.move(elem);
            this.nextRound()
        }
    }

    /**
     *whoBegin
     *
     * Methode qui défini quel joueur commence le jeu (au hasard)
     *
     */
    whoBegin () {
        let start = Math.floor(Math.random()*this.players.length);
        this.round = start; //soit 1 ou 0
        this.currentPlayer = this.players[start];
        this.otherPlayer = this.players[(this.round+1)%2];

        this.currentPlayer.authorizedCell();

        let firstPlayer = document.getElementById('first');
        firstPlayer.textContent = this.currentPlayer.name === "host" ? "Dolhores" : "L'homme en noir";

        //effet rotation cercle à larrière de l'image du joueur dans le panneau
        var currentCircele = document.getElementById(this.currentPlayer.name + '-circele')
        var angle=0;
        this.interval = setInterval(function(){
            currentCircele.style.transform="rotateZ("+ angle++ +"deg)";
        }, 30);


    }

    /**
     *nextRound
     *
     * Methode qui défini le prochain joueur à pouvoir jouer
     *
     */
    nextRound () {
        this.currentPlayer.cleanAuthorizedCell();
        this.endGame();
        this.round++; //on passe au tour suivant
        this.currentPlayer = this.players[this.round%2];
        this.currentPlayer.authorizedCell();


        this.otherPlayer = this.players[(this.round+1)%2];
        this.startFight();
        this.otherPlayer.hideButton();

        //effet rotation cercle image joueur
        clearInterval(this.interval);
        var otherCircele = document.getElementById(this.currentPlayer.name + '-circele')
        var angle=0;
        this.interval = setInterval(function(){
            otherCircele.style.transform="rotateZ("+ angle++ +"deg)";
        }, 30);

        var currentCircele = document.getElementById(this.otherPlayer.name + '-circele')
        var angle=0;
        currentCircele.style.animationPlayState = "stop";


    }

    /**
     *startFight
     *
     * Methode qui démarre le combat (si les 2 joueurs sont côte à côte)
     *
     */
    startFight(){

        var elemCurrentPlayer = document.getElementById(this.currentPlayer.position);
        if (this.currentPlayer.isNotSideOtherPlayers(elemCurrentPlayer) === false && this.isFight === false){

            $("#beginning-fight").fadeIn("slow");
            function eraseFightMessage() {
                $("#beginning-fight").fadeOut("slow");
            }
            setTimeout(eraseFightMessage, 2000);


            this.isFight = true;

        }
        if (this.currentPlayer.isNotSideOtherPlayers(elemCurrentPlayer) === true){
            this.isFight = false
        }
        if (this.currentPlayer.isNotSideOtherPlayers(elemCurrentPlayer) === false){
            this.currentPlayer.showButton();
        }

    }

    /**
     *fight
     *
     * Methode qui définit le combat (perte point de vie après un combat et après une défense)
     *
     */
    fight(){

        var lostHealthFight = null;
        if(this.otherPlayer.isDefended) {
            lostHealthFight = this.otherPlayer.health - (this.currentPlayer.weapon.damage / 2);
            this.otherPlayer.isDefended = false;
        }else{
            lostHealthFight = this.otherPlayer.health - this.currentPlayer.weapon.damage;
        }

        this.otherPlayer.health = lostHealthFight;
        this.otherPlayer.updateHealth();
        this.nextRound();

    }

    /**
     *defend
     *
     * Methode qui définit si le joueur se défend
     *
     */
    defend (){
        this.currentPlayer.isDefended = true;
        this.nextRound();
    }

    /**
     *endGame
     *
     * Methode qui finit le jeu
     *
     */
    endGame () {
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].health <= 0) {
                var endGame = true;
            }

        }
        if (endGame){
            var name = this.currentPlayer.name === "host" ? "Dolhores" : "L'homme en noir";
            document.getElementById('winner-name').textContent = name;
            document.getElementById('end-message').style.display = 'block';
        }
    }
}
