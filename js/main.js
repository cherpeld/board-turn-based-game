/**
 * main.js
 *
 * Script principal de la totalité du jeu (Partie 3)
 *
 * @author Claudine
 * @version 1.0
 * date 01/07/2020
 */

const gameplay = new GamePlay();

let beginningButton = document.getElementById('beginning-button');
let rulesButton = document.getElementById('rules-button');
let backGameButton = document.getElementById('back-button');

beginningButton.addEventListener("click", () => {
    document.getElementById('beginning').style.display = 'none';
    document.getElementById('beginning-game').style.display = 'block';

    //afficher et effacer le message début jeu
    $("#beginning-game").fadeIn("slow");
    function eraseFightMessage() {
        $("#beginning-game").fadeOut("slow");
    }
    setTimeout(eraseFightMessage, 2000);
});

rulesButton.addEventListener("click", () => {
    document.getElementById('rules-page').style.display = 'block';
});

backGameButton.addEventListener("click", () => {
    document.getElementById('rules-page').style.display = 'none';
});

document.addEventListener("click", function(event){

    if (RegExp('[0-9]-[0-9]').test(event.srcElement.id) || RegExp('[0-9]-[0-9]').test(event.path[1].id)) {
        if (event.srcElement.id) {

            gameplay.movePlayer(event.srcElement);

        } else {

            gameplay.movePlayer(event.path[1]) 

        }

    }

    if (event.srcElement.id) {
        var splitId = event.srcElement.id.split('-');

        if (splitId.includes('fight')){
            gameplay.fight()
        }
        if (splitId.includes('defend')) {
            gameplay.defend()
        }
    }

});