/** Class representant les armes */
class Weapons {
    /**
     *
     * Création des armes
     *
     * @param {string} name - nom de l'arme
     * @param {image} image - image de l'arme
     */
    position;
    weapon;
    constructor(name, image, damage) {
        this.name = name;
        this.image = image;
        this.damage = damage;
    }

    /**
     *placeWeapon
     *
     * Methode de la classe Weapons : place les armes
     *
     */
    placeWeapon(emptyCell) {
        this.weapon = document.createElement("img");

        this.weapon.src = this.image;
        this.weapon.className = "image-responsive" + " " + this.name;
        emptyCell.className = "weapon";
        emptyCell.appendChild(this.weapon);
        this.position = emptyCell.id;
    }

}