/** Class representant les cellules du plateau */
class Cellule {
    /**
     *
     * Création des cellules
     *
     * @param {id} id - id de la cellule
     * @class  classe - classe de la cellule
     * @param posX - position X
     * @param posY - position Y
     * @param isAccess - genere le nombre d'obstacles
     *
     */
   constructor(id, classe, posX, posY, isAccess) {
       this.id = id;
       this.classe = classe;
       this.posX = posX;
       this.posY = posY;
       this.isAccess = Math.random() <= 0.9 ? true : false ; // genere environ 10 obstacles
   }

}
